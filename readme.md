# qrcodeRecognitionPython
A Python program that recognizes, rectifies and cuts QR codes in images using the OpenCV module.
### Usage
To use the program the libraries `numpy`, `opencv-python`, `shapely`and `pyzbar` must be installed. The program can then be run like this:
```bash
python3 main.py -i ImagePath
```
