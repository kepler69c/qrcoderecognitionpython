from ast import pattern
import cv2
import numpy as np
import math
from pyzbar.pyzbar import decode
import os
from collections import OrderedDict
from shapely.geometry import Point, Polygon

# finds qr codes
def find_code(img, img2, img3):
    cvt = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY )

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    grad = cv2.morphologyEx(cvt, cv2.MORPH_GRADIENT, kernel)

    _, thresh = cv2.threshold(grad, 0.0, 255.0, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

# with the contour all connected rectangles are found
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 1))
    connected = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

    contours, hierarchy = cv2.findContours(connected.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[-2:]
    qr_arr = []

# the found rectangles are checked if they have the right size and shape
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        # get rotated rectangle from contour
        rot_rect = cv2.minAreaRect(c)
        box = cv2.boxPoints(rot_rect)
        box = np.int0(box)
        cv2.drawContours(img2,[box],0,(255,36,122),2)

        if math.isclose(w/h, h/w, abs_tol=0.1) and h>=7 and w>=7:
                cv2.drawContours(img2,[box],0,(36,255,122),3)
                cv2.drawContours(img3,[box],0,(255,36,122),2)
                qr_arr.append([x, y, w, h, box])

    return cvt, thresh, connected, qr_arr, contours

# function finds position markers within the possible qr codes
def find_pattern(img3, cvt, qr_arr):
    img_thresh = cv2.adaptiveThreshold(cvt, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY, 51, 0)
    kernel = np.ones((5, 5), 'uint8')
    erode_img = cv2.erode(img_thresh, kernel, iterations=1)
    edges_img = cv2.Canny(erode_img, 50, 200)

    contours2, hierarchy = cv2.findContours(edges_img.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    pattern_arr = []

    # cycle through contours to find square-shaped objects
    for cnt in contours2:
        approx = cv2.approxPolyDP(cnt, 0.03*cv2.arcLength(cnt, True), True)

        if len(approx) == 4:
            x, y, w, h = cv2.boundingRect(cnt)
            # if a pattern is at least 7 pixels wide it could be correct
            if w >= 7 and h >= 7:
                if math.isclose(w, h, abs_tol=3):
                    pattern_arr.append([x,y,w,h])
                    cv2.rectangle(img3, (x, y), (x + w, y + h), (36,255,12), 3)

    return pattern_arr, img_thresh, erode_img, edges_img, contours2

# part of the verify_pattern() function. loops through a pixel column until the color changes
def array_loop(array, posY, posX, a, change):
    try:
        length = 0
        while array[posY][posX] != change:
            length += 1
            if a == "x":
                posX += 1
            elif a == "y":
                posY += 1
        
    except:
        pass

    if a == "x":
        a = posX
    elif a == "y":
        a = posY
    return length, a

# checks the position markers found in the function find_pattern for the 1:1:3:1:1 ratio
def verify_pattern(img, arr, thresh):
    imgH, imgV = img.copy(), img.copy()
    img_arr, err_arr = np.array(thresh), []

    for pattern in range(len(arr)):
        # horizontal pattern ratio detection (1:1:3:1:1)
        x = arr[pattern][0]
        x0 = x
        y0 = arr[pattern][1]
        y = y0 + round(arr[pattern][3]/2)
        w = arr[pattern][2]
        h = arr[pattern][3]

        # to stop program from crashing if the pattern is on the border of the image
        if img_arr[y][x] < 10 or x+w > 450 or y+h > 450:# livecam frame is 460 px wide
            err_arr = [pattern] + err_arr
            continue

# the middle vertical pixel row of the possible position marker is scrolled pixel by pixel
# the succession of black to white pixels must be proportional to 1:1:3:1:1
        if img_arr[y][x] == 255:
            x1, x0 = array_loop(img_arr, y, x0, "x", 0)#white border
        else:
            x1 = 0
        x2, x0 = array_loop(img_arr, y, x0, "x", 255)#black   1
        x3, x0 = array_loop(img_arr, y, x0, "x", 0)#white     1
        x4, x0 = array_loop(img_arr, y, x0, "x", 255)#black   3
        x5, x0 = array_loop(img_arr, y, x0, "x", 0)#white     1
        x6, x0 = array_loop(img_arr, y, x0, "x", 255)#black   1

        r1 = math.isclose(x2, x3, abs_tol=2)
        r2 = math.isclose(x5, x6, abs_tol=2)
        r3 = math.isclose((x2+x3)/2, x4/3, abs_tol=2)
        r4 = math.isclose((x5+x6)/2, x4/3, abs_tol=2)

        cv2.rectangle(imgH, (x, y), (x + w, y), (255, 0, 12), 3)
        cv2.rectangle(imgH, (x, y0), (x + w, y0 + h), (36,255,12), 3)

        if r1 and r2 and r3 and r4:
            pass
            cv2.rectangle(imgV, (x, y), (x + w, y), (255, 0, 12), 3)
            cv2.rectangle(imgV, (x, y0), (x + w, y0 + h), (36,255,12), 3)
        else:
            err_arr = [pattern] + err_arr

    err_arr = sorted(err_arr, reverse=True)
    for err in range(len(err_arr)):
        arr.pop(err_arr[err])
    err_arr = []

# the position markers are checked again; this time vertically
# double-checking is necessary because qr-codes can be very distorted
    for i in range(len(arr)):
        x0 = arr[i][0]
        x = x0 + round(arr[i][2]/2)
        y = arr[i][1]
        y0 = y
        w = arr[i][2]
        h = arr[i][3]

        if img_arr[y][x] == 255:
            y1, y0 = array_loop(img_arr, y0, x, "y", 0)#white border
        else:
            y1 = 0
        y2, y0 = array_loop(img_arr, y0, x, "y", 255)#black   1
        y3, y0 = array_loop(img_arr, y0, x, "y", 0)#white     1
        y4, y0 = array_loop(img_arr, y0, x, "y", 255)#black   3
        y5, y0 = array_loop(img_arr, y0, x, "y", 0)#white     1
        y6, y0 = array_loop(img_arr, y0, x, "y", 255)#black   1

        r1 = math.isclose(y2, y3, abs_tol=2)
        r2 = math.isclose(y5, y6, abs_tol=2)
        r3 = math.isclose((y2+y3)/2, y4/3, abs_tol=2)
        r4 = math.isclose((y5+y6)/2, y4/3, abs_tol=2)

        if r1 and r2 and r3 and r4:
            pass
        else:
            err_arr = [i] + err_arr

        cv2.rectangle(imgV, (x, y), (x, y + h), (12, 0, 255), 3)
        cv2.rectangle(imgV, (x0, y), (x0 + w, y + h), (36,255,12), 3)

    err_arr = sorted(err_arr, reverse=True)
    for err in range(len(err_arr)):
        arr.pop(err_arr[err])

    return arr, imgH, imgV

# this function sorts the points (vertices) of a rectangle in descending order
def order_points(pts):
	rect = np.zeros((4, 2), dtype = "float32")
	s = pts.sum(axis = 1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]

	diff = np.diff(pts, axis = 1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]
	# return the ordered coordinates
	return rect

# do the possible qr codes contain position markers?
def verify_code(img4, qr_arr, pattern_arr, contours):
    qr_verified = []

    for idx in range(len(qr_arr)):
        x, y, w, h, box = qr_arr[idx]

        for i in range(len(pattern_arr)):
            xp = pattern_arr[i][0] + round(pattern_arr[i][2]/2)
            yp = pattern_arr[i][1] + round(pattern_arr[i][3]/2)

            # create polygon (around qr code) and check if polygon contains point
            polygon = Polygon(box)
            point = Point(xp,yp)

            if polygon.contains(point):
                qr_verified.append(box)

    uniq = []
    for i in qr_verified:
        if not i in uniq:
            uniq.append(i)

    for i in range(len(uniq)):
        cv2.rectangle(img4, (x, y), (x+w-1, y+h-1), (225, 225, 225), 2)
        cv2.polylines(img4, uniq, True, (255,255,255), 2)

    return uniq

# qr codes are usually slightly distorted because they are photographed. this function rectifies them
def four_point_transform(img, qr_verified):
    rect = order_points(qr_verified)
    (tl, tr, br, bl) = rect

    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype = "float32")

    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(img, M, (maxWidth, maxHeight))

    return warped

# recognized and confirmed qr codes are cropped out
def cut_code(img, img4, pattern_arr, qr_verified):
    msg = 'No QR-Code found'
    if len(qr_verified) == 1:
        print(qr_verified)
        qr_img = four_point_transform(img, qr_verified[0])
        msg = 'found qr-code'
        return qr_img, msg
    return img4, msg

# the cut qr codes are decoded with pyzbar
def read_code(qr_verified, qr_img):
    try:
        for i in range(len(qr_verified)):
            x, y, w, h = qr_verified[i]

        decoded = decode(qr_img)
        return decoded
    except:
        print('no QR-Code found')