import cv2
import numpy as np
import argparse
import os
# other python file
import detect

# Some Qr codes as an example
# large.png
# sticker.jpg
# covid.png
# qr_silver.png

if __name__ == "__main__":
    # define, parse arguments
    ap = argparse.ArgumentParser(description='recognize and decode QR-Codes')
    ap.add_argument("-i", "--image", help="path to the image file (whitout img/)")
    ap.add_argument("-s", "--suppress", action="store_false", help="suppress steps and only show result")
    args = vars(ap.parse_args())

    # choose an image
    if args["image"]:
        img = cv2.imread('img/'+args["image"])
    else:
        img = cv2.imread('img/sticker.jpg')
    img2, img3, img4 = img.copy(), img.copy(), img.copy()

    # find QR-Code, position patterns
    cvt, thresh, connected, qr_arr, contours = detect.find_code(img, img2, img3)
    pattern_arr, thresh2, erode_img, edges_img, contours2 = detect.find_pattern(img3, cvt, qr_arr)

    # verify QR-Code, patterns
    pattern_arr, imgH, imgV = detect.verify_pattern(img, pattern_arr, thresh2)
    qr_verified = detect.verify_code(img4, qr_arr, pattern_arr, contours)

    # cut out QR-Code, read it
    qr_img, msg = detect.cut_code(img, img4, pattern_arr, qr_verified)
    if msg == 'found qr-code':
        decoded = detect.read_code(qr_verified, qr_img)

        print(decoded[0].data.decode("utf-8"))
        os.system('notify-send ' + decoded[0].data.decode("utf-8"))
    else:
        print(msg)

    if args["suppress"]:
        # show results from finding the qr code
        thresh = cv2.cvtColor(thresh, cv2.COLOR_GRAY2RGB)
        connected = cv2.cvtColor(connected, cv2.COLOR_GRAY2RGB)

        horizontal_1 = np.concatenate((img, thresh), axis=1)
        horizontal_2 = np.concatenate((connected, img2), axis=1)
        vertical_1 = np.concatenate((horizontal_1, horizontal_2), axis=0)

        cv2.namedWindow('inital image / thresh / clean / result', cv2.WINDOW_KEEPRATIO)
        cv2.imshow('inital image / thresh / clean / result', vertical_1)
        cv2.resizeWindow('inital image / thresh / clean / result', 1000, 1000)

        # show results from finding the qr code patterns
        thresh2 = cv2.cvtColor(thresh2, cv2.COLOR_GRAY2RGB)

        horizontal_3 = np.concatenate((thresh2, img3), axis=1)
        horizontal_4 = np.concatenate((imgH, imgV), axis=1)
        vertical_2 = np.concatenate((horizontal_3, horizontal_4), axis=0)

        cv2.namedWindow('threshold / patterns / verification1 / verification2', cv2.WINDOW_KEEPRATIO)
        cv2.imshow('threshold / patterns / verification1 / verification2', vertical_2)
        cv2.resizeWindow('threshold / patterns / verification1 / verification2', 1000, 1000)

        print(contours)

    # imshow
    cv2.imshow("result4", img4)
    try:
        cv2.imshow('qr-code', qr_img)
    except:
        print('no QR-Code found!')
    cv2.waitKey()
